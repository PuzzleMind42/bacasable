package Charrier.Quentin.IL;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;

/**
 * Hello world!
 *
 */
public class App 
{
    @NotNull
    @Size(min=1)
    private String firstName;
    @NotNull
    @Size(min=1)
    private String lastName;
    @NotNull    //(groups = {AdultCheck.class})
    private Date birthDate;
    private String citizenship;
    @NotNull
    @Min(value=18)
    private transient Integer age;

    public App(String fn, String ln, Date d, String cs){
        this.firstName = fn;
        this.lastName = ln;
        this.birthDate = d;
        this.age = 21;
        this.citizenship = cs;
    }
}
