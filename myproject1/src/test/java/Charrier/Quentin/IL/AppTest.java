package Charrier.Quentin.IL;

import static junit.framework.TestCase.assertEquals;
import static org.junit.Assert.assertTrue;
import org.junit.Test;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.Calendar;
import java.util.Date;
import java.util.Set;

/**
 * Unit test for simple App.
 */
public class AppTest 
{
    /**
     * Rigorous Test :-)
     */
    @Test
    public void shouldAnswerWithTrue()
    {
        Calendar calendar = Calendar.getInstance();
        calendar.set(1997,Calendar.DECEMBER,23);
        Date d = calendar.getTime();
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        Validator validator = factory.getValidator();
        App p = new App("Jean","Paul", d, "France");
        Set<ConstraintViolation<App>> constraintViolation = validator.validate(p);
        assertEquals(0, constraintViolation.size());
    }
}
